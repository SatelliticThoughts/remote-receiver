#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#ifndef SERVER_H
#define SERVER_H

typedef struct server Server;

struct Server* server_new();
void server_run(struct Server* s);

#endif
