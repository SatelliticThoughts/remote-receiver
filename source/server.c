#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#define PORT 8080
#define MAXLINE 1024

struct Server {
	int file_descriptor;
	char buffer[MAXLINE];
	struct sockaddr_in s_address;
	struct sockaddr_in c_address;
};

struct Server* server_new() {
	int fd = socket(AF_INET, SOCK_DGRAM, 0);
	// Creating socket file descriptor
    if(fd < 0) {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }

	struct sockaddr_in saddr, caddr;
    memset(&saddr, 0, sizeof(saddr));
    memset(&caddr, 0, sizeof(caddr));

    // Filling server information
    saddr.sin_family = AF_INET; // IPv4
    saddr.sin_addr.s_addr = INADDR_ANY;
    saddr.sin_port = htons(PORT);

    // Bind the socket with the server address
    if (bind(fd, (const struct sockaddr *)&saddr,
            sizeof(saddr)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
	
	struct Server* s = malloc(sizeof(s));
	s->file_descriptor = fd;
	s->s_address = saddr;
	s->c_address = caddr;
	free(s);
	return s;
}

void server_run(struct Server* s) {
	socklen_t len;
	int n = recvfrom(
				s->file_descriptor,
				(char *)s->buffer,
				MAXLINE,
				MSG_WAITALL,
				(struct sockaddr *) &s->c_address,
				&len);

	s->buffer[n] = '\0';
}
