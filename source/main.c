#include "cmd.h"
#include "csv.h"
#include "server.h"

#include <stdio.h>

int main(int argc, char *argv[]) {
	printf("Hello\n");
	cmd_new();
	csv_new();
	struct Server* s = malloc(sizeof(s));
	server_new(&s);
	
	while(1) {
		server_run(s);
		char* cmd = csv_get_cmd(s->buffer);
		//cmd_run(cmd);
	}
	return 0;
}
