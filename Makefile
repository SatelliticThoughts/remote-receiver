CC=gcc -g
IDIR=include
CFLAGS=-Wall -I $(IDIR)
LDFLAGS=
TARGET=remotereceiver
VPATH=source
OBJDIR=binary

_DEPS = cmd.h csv.h server.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = main.o cmd.o csv.o server.o
OBJ = $(patsubst %,$(OBJDIR)/%,$(_OBJ))

all: $(TARGET)

$(OBJDIR):
	mkdir -p $(OBJDIR)
	
$(OBJDIR)/%.o: %.c $(DEPS) | $(OBJDIR)
	$(CC) $(CFLAGS) -c -o $@ $< $(LDFLAGS)

$(TARGET): $(OBJ)
	gcc $(CFLAGS) -o $@ $^

clean:
	rm -f $(OBJ) $(TARGET)
